##############################
BauerNet Bootstrapping Modules
##############################

This repository contains modules related to the first-time bootstrap of a BauerNet or
model infrastructure.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
