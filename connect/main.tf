variable "bootstrap_host" {
  type = string
}
variable "bootstrap_username" {
  type = string
}

data "external" "node_token" {
  program = ["${path.module}/get-remote-file.sh"]

  query = {
    host     = var.bootstrap_host
    username = var.bootstrap_username
    file     = "/var/lib/rancher/k3s/server/token"
  }
}

locals {
  # Use this instead of the data source directly. See:
  # https://github.com/hashicorp/terraform/issues/29331
  node_token = sensitive(data.external.node_token.result.content)
}

data "kubernetes_namespace" "kube_system" {
  metadata {
    name = "kube-system"
  }
}

resource "kubernetes_secret" "node_token" {
  metadata {
    namespace = one(data.kubernetes_namespace.kube_system.metadata).name
    name      = "node-token"
  }
  data = {
    token = local.node_token
  }
}

output "node_token" {
  value     = local.node_token
  sensitive = true
}
