#!/bin/bash

set -eu

TARGET="$1"
printf "%s" "Waiting for $TARGET to respond to ping ..."
while ! ping -c 1 -n -w 1 "$TARGET" &> /dev/null
do
    printf "%c" "."
done
printf " %s\n" "OK"
