Connect
=======

This bootstrap Terraform component is responsible for retrieving the kubeconfig
file, connecting to a bootstrap single-node cluster for the first time, and
ensuring it is correctly configured.

Terragrunt Configuration
------------------------

.. literalinclude:: terragrunt.hcl
  :caption: ``terragrunt.hcl``

Refresh Kubeconfig
^^^^^^^^^^^^^^^^^^

In order to retrieve the kubeconfig necessary for Terraform to connect to the
cluster, we make use of a Terragrunt ``before_hook``, which uses ``ssh`` to
connect to the node, elevate to root, and retrieve the local administrator's
kubeconfig.

Prior to doing this, it extracts the certificate authority data from the local
kubeconfig and initiates a connection to the existing cluster, to see if the
issuer matches. If it does, it skips further execution (by the logic that if
the CA matches, the script has likely already been run.)

.. literalinclude:: refresh-kubeconfig.sh
  :caption: ``refresh-kubeconfig.sh``
  :language: bash

Terraform Configuration
-----------------------

.. literalinclude:: main.tf
  :caption: ``main.tf``

Get Remote File External Data Source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Terraform supports "external" data sources, which are simply programs naively
executed by Terraform, expecting a specific contract for ``stdin`` and
``stdout``, to parse the data. Full documentation is available `here <https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/data_source>`_.

Retrieving the ``node-token`` for subsequent node joins requires reaching out
to the filesystem of an existing node. We take this step using the
``get-remote-file.sh`` script, which fulfills the external data contract, and
is able to non-interactively retrieve the file contents.

.. literalinclude:: get-remote-file.sh
  :caption: ``get-remote-file.sh``
  :language: bash
