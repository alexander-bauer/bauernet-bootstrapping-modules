#!/bin/bash

set -eu

USER="$1"; shift
HOST="$1"; shift
KUBECONFIG="$1"; shift

CA="$(awk '/certificate-authority-data/ {print $2}' "$KUBECONFIG" | base64 -d)"

if [[ -n "$CA" ]]; then
    echo "Checking $HOST:6443 with existing kubeconfig CA"
  set +e # do not exit on nonzero exit code
  CA_OK="$(echo '' | \
    openssl s_client -connect "$HOST:6443" \
    -CAfile <(echo "$CA") 2>&1 \
    | grep -icE "verification.*ok")"
  set -e # back to normal
  if [[ "$CA_OK" -gt 0 ]]; then
      echo "Current certificate is valid. Skipping." 1>&2
      exit 0
  else
    echo "CA does not match, using ssh to get new kubeconfig" 1>&2
  fi
fi

echo "Retrieving kubeconfig from $HOST"
CONTENT=$(ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
  -l "$USER" "$HOST" "sudo cat /etc/rancher/k3s/k3s.yaml")

echo "Writing kubeconfig locally"
echo "${CONTENT//127.0.0.1/$HOST}" > "$KUBECONFIG"
